$(function() {
    console.log("my bootstarp and jquery working");

    $(window).on("scroll", function() {
        if ($(window).scrollTop() >= 133) {
          $(".navbar").addClass("scroll");
        } else {
          $(".navbar").removeClass("scroll");
        }
      });
    
    //reset hamburger menu toggle
    $(window).resize(function(e) {
        $("#openSidebarMenu").prop('checked', false);
    });

    // show modal home page
    // $('#promotionModal').modal('show')

    //load hhtml not working
    $('.uv2-nav li').click(function() {
        $('.uv2-nav li .anormal').removeClass('active'); 
        $(this).addClass('active');
    });

     // ======== for validate form request contact ======== //
    window.addEventListener('load', () => {

      // Grab all the forms
      var forms = document.getElementsByClassName('needs-validation');
      
      // Iterate over each one
      for (let form of forms) {
        // Add a 'submit' event listener on each one
        form.addEventListener('submit', (evt) => {
          // check if the form input elements have the 'required' attribute  
          if (!form.checkValidity()) {
            evt.preventDefault();
            evt.stopPropagation();
            console.log('Bootstrap will handle incomplete form fields');
          } else {
            // Since form is now valid, prevent default behavior..
            evt.preventDefault();
            console.info('All form fields are now valid...');
          }
          form.classList.add('was-validated');
        });
      }
    });

    // ======== for toggle products page collapse ======== //
    $('.collapse').on('show.bs.collapse', function () {
      $('.collapse.show').each(function(){
          $(this).collapse('hide');
      });
    });

    // ======== for products quantity up and down button ======== //
    // $('<div class="quantity-nav"><div class="quantity-button quantity-up"><i class="fas fa-chevron-up"></i></div><div class="quantity-button quantity-down"><i class="fas fa-chevron-down"></i></div></div>').insertAfter('.products-quantity input');
    $('<div class="quantity-nav"><div class="quantity-button quantity-up"><img src="assets/icons/up.png" alt=""></div><div class="quantity-button quantity-down"><img src="assets/icons/down.png" alt=""></div></div>').insertAfter('.products-quantity input');
    
    $('.products-quantity').each(function() {
      var spinner = $(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });

    $('.quantity').each(function() {
      var spinner = $(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
      });

    });

    $('.thumb').click(function(){
      $(this).addClass('active').siblings().removeClass('active');
    });

    
    $('.opensearch').click(function(){
      document.getElementById("myOverlay").style.display = "block";
    })

    $('.closesearch').click(function(){
      document.getElementById("myOverlay").style.display = "none";
    })
});